package rgalati.ch.massagetherapeutique.base;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Roberto on 08.06.2017.
 */
public class GetKeysFromPrivateKey extends AsyncTask<String, Void, JSONObject> {
    private String privateAddress;
    private String amount;
    @Override
  /* Récupération de l'object JSON à partir de l'URL donné en paramètre sous forme d'un String */
    protected JSONObject doInBackground (String... params) {
        try {
            privateAddress = params[0];
            amount = params[1];
            URL address = new URL("http://groups.cowaboo.net/group3/public/api/user?secretKey="+privateAddress);
            HttpURLConnection connection = (HttpURLConnection)address.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true); connection.setDoOutput(false); /* Valeurs par défaut: donc inutile ici! */

            InputStream in = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line = reader.readLine();
            while (line != null) {
                result.append(line).append("\n");
                line = reader.readLine();
            }
            in.close();
            connection.disconnect();
            return new JSONObject(result.toString());
        } catch (Exception e) {
            return null;
        }
    } // doInBackground

    @Override
  /* Le résultat est transmis au listener */
    protected void onPostExecute (JSONObject result) {
        try {
            String publicAddress = (String)result.get("publicAddress");
            new StellarDebit().execute(publicAddress, privateAddress, amount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    } // onPostExecute
}
