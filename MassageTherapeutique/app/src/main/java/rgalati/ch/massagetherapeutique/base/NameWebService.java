package rgalati.ch.massagetherapeutique.base;

import android.util.Log;

/**
 * Created by Roberto Galati/Sebastien Nicollin on 04.05.2017.
 */
public class NameWebService {
    public static final String KEY = "SAN3GE7PHBTX7XJDGPW3ZPID5T4WHF7HUPVK2PROCQE3XGZLAO5FTMD6"; // Secret Key Cowaboo
    public static final String URL_BASE = "http://groups.cowaboo.net/group10/public/api/";

    /* GetTags */
    public static final String OBSERVATORY = "observatory";
    public static final String OBSERVATORYID = "observatoryId";

    public static final String DICTIONARY = "dictionary";
    public static final String ID = "id";
    public static final String ENTRIES = "entries";
    public static final String TAGS = "tags";
    public static final String VALUE = "value";

    /* Commandes */
    public static final String GETTAG = URL_BASE + "tags"; /* Récupération de toutes les informations */
    public static final String GETTVALUES = URL_BASE + OBSERVATORY + "?" + OBSERVATORYID + "=";

    public static final String THERAPEUTE = "||Find Massage||";

}
