package rgalati.ch.massagetherapeutique.domaine;

import android.util.Log;

/**
 * Created by Roberto Galati/Sebastien Nicollin on 04.05.2017.
 */
public class Therapeute implements Comparable<Therapeute> {
    private String nom;
    private String adresse;
    private String npa;
    private String ville;

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setNpa(String npa) {
        this.npa = npa;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setInfos(String value){
        String[] splitedValues = value.split(",");
        setAdresse(splitedValues[0]);
        setNpa(splitedValues[1]);
        setVille(splitedValues[2]);
        Log.i("infoo", "setData1: " + value);
    }

    public Therapeute(String nom){
        this.nom = nom; this.adresse = ""; this. npa = ""; this.ville = "";
    }

    public String getNom() {
        return nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getNpa() {
        return npa;
    }

    public String getVille() {
        return ville;
    }

    @Override
    public String toString() {
        return "Therapeute{" +
                "nom='" + nom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", npa='" + npa + '\'' +
                ", ville='" + ville + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o){
        return nom.equals(((Therapeute)o).getNom());
    }

    @Override
    public int compareTo(Therapeute t){
        return nom.compareTo(t.nom);
    }

}
