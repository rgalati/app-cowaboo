package rgalati.ch.massagetherapeutique.metier;

import rgalati.ch.massagetherapeutique.domaine.Therapeute;

/**
 * Created by Roberto on 08.06.2017.
 */
public class Donnees {
    private static Donnees instance =  new Donnees();
    public static Donnees getInstance(){return instance;}
    private Therapeute therapeute;
    public void setTherapeute(Therapeute t){
        this.therapeute = t;
    }
    public Therapeute getTherapeute(){return therapeute;}
}
