package rgalati.ch.massagetherapeutique.metier;

import android.util.Log;

import rgalati.ch.massagetherapeutique.domaine.Therapeute;

/**
 * Created by Roberto Galati/Sebastien Nicollin on 04.05.2017.
 */
public class FiltreTherapeute implements FiltreTherapeuteInterface {
    private String value;

    public FiltreTherapeute(String value){
        this.value = value.toLowerCase();
    }

    public boolean isIn(Therapeute therapeute){
        if(therapeute.getVille().toLowerCase().contains(value) || therapeute.getNpa().contains(value)){
            return true;
        }
        return false;
    }

    public void setValue(String value){
        this.value = value;
    }
}
