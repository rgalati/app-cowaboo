package rgalati.ch.massagetherapeutique.metier;

import rgalati.ch.massagetherapeutique.domaine.Therapeute;

/**
 * Created by Roberto Galati/Sebastien Nicollin on 04.05.2017.
 */
public interface FiltreTherapeuteInterface {
    public boolean isIn(Therapeute therapeute);
}
