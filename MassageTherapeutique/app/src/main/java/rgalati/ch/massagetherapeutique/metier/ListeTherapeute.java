package rgalati.ch.massagetherapeutique.metier;

import android.content.Context;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import rgalati.ch.massagetherapeutique.R;
import rgalati.ch.massagetherapeutique.domaine.Therapeute;

/**
 * Created by Roberto Galati/Sebastien Nicollin on 04.05.2017.
 */
public class ListeTherapeute {
    public static final String REF_THERAPEUTE = "Therapeute";
    private static final String[] FROM = {"Nom", "Adresse", "Npa", "Ville"};
    private static final int[] TO = {R.id.tvNom, R.id.tvAdresse, R.id.tvNpa, R.id.tvVille};

    private List<HashMap<String, Object>> dataTout, dataFiltre;
    private SimpleAdapter adapter;
    private FiltreTherapeute filtre = new FiltreTherapeute("");


    public ListeTherapeute(Context context, Collection<Therapeute> lst_thera){
        TreeSet<Therapeute> therapeutes = new TreeSet<>(lst_thera);
        dataTout = new ArrayList<>(therapeutes.size());
        for (Therapeute therapeute: therapeutes){
            HashMap<String, Object> map = new HashMap<>();
            map.put(FROM[0], therapeute.getNom());
            map.put(FROM[1], therapeute.getAdresse());
            map.put(FROM[2], therapeute.getNpa());
            map.put(FROM[3], therapeute.getVille());
            map.put(REF_THERAPEUTE, therapeute);
            dataTout.add(map);
        }
        dataFiltre = new ArrayList<>(therapeutes.size());
        adapter = new SimpleAdapter(context, dataFiltre, R.layout.un_therapeute, FROM, TO);
    }


    public SimpleAdapter getAdapter () {return adapter;}

    public void doFiltre(String value){
        setFiltre(value);
        dataFiltre.clear();
        for (HashMap<String, Object> hm : dataTout){
            Therapeute t = (Therapeute) hm.get(REF_THERAPEUTE);
            if (filtre.isIn(t)){
                dataFiltre.add(hm);
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void setFiltre(String value){
        filtre.setValue(value);
    }


}
