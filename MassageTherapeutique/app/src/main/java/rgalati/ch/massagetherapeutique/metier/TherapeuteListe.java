package rgalati.ch.massagetherapeutique.metier;

import java.util.ArrayList;
import java.util.Collection;

import rgalati.ch.massagetherapeutique.domaine.Therapeute;

/**
 * Created by Roberto on 04.05.2017.
 */
public class TherapeuteListe {
    public TherapeuteListe(){}

    private Collection<Therapeute> therapeutes = new ArrayList<Therapeute>();

    public void addTherapeute(Therapeute t) {therapeutes.add(t);}

    public Therapeute getTherapeute(String name) {
        for ( Therapeute t : therapeutes) {
            if (t.equals(new Therapeute(name))) {
                return t;
            }
        }
        return null;
    }
    public Collection<Therapeute> getTherapeutes() {return therapeutes;}
    public void removeTherapeutes() { therapeutes.clear();}
}