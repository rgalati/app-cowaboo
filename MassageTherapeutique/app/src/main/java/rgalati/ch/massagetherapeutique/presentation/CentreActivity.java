package rgalati.ch.massagetherapeutique.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import rgalati.ch.massagetherapeutique.R;
import rgalati.ch.massagetherapeutique.base.GetKeysFromPrivateKey;
import rgalati.ch.massagetherapeutique.domaine.Therapeute;
import rgalati.ch.massagetherapeutique.metier.Donnees;
import rgalati.ch.massagetherapeutique.metier.ListeTherapeute;

/**
 * Created by Roberto on 08.06.2017.
 */
public class CentreActivity extends AppCompatActivity {
    private TextView tvNom, tvAdresse, tvNpa, tvLocalite;
    private EditText etMontant, etCleSecrete;
    private Button btnAjout;
    private Donnees donnees;
    String secretKeyRob = "SAN3GE7PHBTX7XJDGPW3ZPID5T4WHF7HUPVK2PROCQE3XGZLAO5FTMD6"; // Clé de Roberto pour éviter de tout taper

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.centre);
        donnees = Donnees.getInstance();
        definirControles();
        definirListeners();
    } // onCreate

    private void definirControles(){
        tvNom = (TextView)findViewById(R.id.tvNom);
        tvAdresse = (TextView)findViewById(R.id.tvAdresse);
        tvNpa = (TextView)findViewById(R.id.tvNpa);
        tvLocalite = (TextView)findViewById(R.id.tvLocalite);
        etMontant = (EditText)findViewById(R.id.etMontant);
        etCleSecrete = (EditText)findViewById(R.id.etCleSecrete);
        btnAjout = (Button)findViewById(R.id.btnAjout);
        btnAjout.setEnabled(false);
        Therapeute t = donnees.getTherapeute();
        tvNom.setText(t.getNom());
        tvAdresse.setText(t.getAdresse() + " - ");
        tvNpa.setText(t.getNpa() + " ");
        tvLocalite.setText(t.getVille());
        etCleSecrete.setText(secretKeyRob);
    }

    private void definirListeners(){
        etMontant.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                validerEditText();
            }
        });
        btnAjout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String secretKey = etCleSecrete.getText().toString().trim();
                String amount = etMontant.getText().toString().trim();
                new GetKeysFromPrivateKey().execute(secretKeyRob, amount);
                int duration = Toast.LENGTH_LONG;
                String text = "Votre paiement de " + etMontant.getText().toString() + " Stellar a bien été effectué !";
                Toast toast = Toast.makeText(getApplicationContext(), text,duration);
                toast.show();
                resetChampsText();
            }
        });
        etCleSecrete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                validerEditText();
            }
        });
    }

    @Override
    public void onBackPressed(){
        resetChampsText();
        finish();
    }
    private void validerEditText(){
        if(etMontant.getText().toString().equals("") || etCleSecrete.getText().toString().equals("")){btnAjout.setEnabled(false);}else{btnAjout.setEnabled(true);}
    }
    private void resetChampsText(){
        etMontant.setText("");
        etCleSecrete.setText("");
    }
}
