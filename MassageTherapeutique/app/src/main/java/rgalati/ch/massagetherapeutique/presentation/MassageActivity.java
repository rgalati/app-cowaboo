package rgalati.ch.massagetherapeutique.presentation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.TreeSet;

import rgalati.ch.massagetherapeutique.R;
import rgalati.ch.massagetherapeutique.base.GetFromUrl;
import static rgalati.ch.massagetherapeutique.base.NameWebService.*;
import rgalati.ch.massagetherapeutique.domaine.Therapeute;
import rgalati.ch.massagetherapeutique.metier.Donnees;
import rgalati.ch.massagetherapeutique.metier.ListeTherapeute;
import rgalati.ch.massagetherapeutique.metier.TherapeuteListe;

/**
 * Module 635.1 - Programmation
 * Activité principale
 *
 * @author Roberto Galati/Sebastien Nicollin - HEG-Genève
 * @version Version 1.0
 */
public class MassageActivity extends AppCompatActivity implements GetFromUrl.Listener {
    private TextView tvLocalite;
    private EditText etLocalite;
    private ListView lvTherapeute;
    private ListeTherapeute listeTherapeute;
    private Donnees donnees;

  @Override
  protected void onCreate (Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
      donnees = Donnees.getInstance();
      definirControles();
      definirListeners();
      getInfo();
    } // onCreate

    private void definirControles(){
        tvLocalite = (TextView) findViewById(R.id.tvLocalite);
        etLocalite = (EditText) findViewById(R.id.etLocalite);
        lvTherapeute = (ListView)findViewById(R.id.lvTherapeute);
    }


    private void init(){
        listeTherapeute = new ListeTherapeute(getApplicationContext(), therapeutesListe.getTherapeutes());
        listeTherapeute.doFiltre(etLocalite.getText().toString().trim().toLowerCase());
        lvTherapeute.setAdapter(listeTherapeute.getAdapter());
    }

    private void definirListeners(){
        etLocalite.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                init();
            }
        });

        lvTherapeute.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, Object> hm = (HashMap<String, Object>)parent.getItemAtPosition(position);
                Therapeute t = (Therapeute)hm.get(ListeTherapeute.REF_THERAPEUTE);
                donnees.setTherapeute(t);
                Intent intent = new Intent(getApplicationContext(), CentreActivity.class);
                startActivity(intent);
            }
        });
    }

    public static TherapeuteListe therapeutesListe= new TherapeuteListe();

    public class AllTherapeutes{
        public AllTherapeutes(JSONObject json, MassageActivity main) throws JSONException {
            JSONObject list = json.getJSONObject("tag_list");
            JSONObject lst_therapeutes = list.getJSONObject("list");
            Log.i("infoo", "lst_thera: " + lst_therapeutes);
            Iterator<String> iterator = lst_therapeutes.keys();
            therapeutesListe.removeTherapeutes();
            while (iterator.hasNext()) {
                Therapeute t = new Therapeute(iterator.next());
                therapeutesListe.addTherapeute(t);
            }

            for (Therapeute t: therapeutesListe.getTherapeutes()) {
                String str = t.getNom();
                if(str.contains(" ")){
                    str.replaceAll(" ", "%20");
                    Log.i("infoo", "str: " + str);
                }else{
                    Log.i("infoo", "no Data!");
                }
                new GetFromUrl(main, 2).execute(GETTVALUES + t.getNom());
            }

        }
    }

    public class SetDataTherapeute{
        public SetDataTherapeute(JSONObject json) throws JSONException{
            JSONObject dictionary = json.getJSONObject(DICTIONARY);
            String nom = dictionary.getString(ID);
            Therapeute t = therapeutesListe.getTherapeute(nom);
            if(t != null){
                JSONObject entries = dictionary.getJSONObject(ENTRIES);
                Iterator<String> iterator = entries.keys();
                String idEntry = iterator.next();
                JSONObject entry = entries.getJSONObject(idEntry);
                String value = entry.getString(VALUE);
                t.setInfos(value);
            }
        }
    }

    @Override
    public void onGetFromUrlResult (JSONObject json) {
        try{
            int type = json.getInt("type");

            switch (type){
                case 1:
                    new AllTherapeutes(json, this);
                    break;
                case 2:
                    Log.i("infoo", "IM HEEERRREEE: " + json);
                    new SetDataTherapeute(json);
                    break;
            }
        } catch (JSONException e) { System.out.println("Erreur"); return;}
    } // onGetFromUrlResult

    @Override
    /* Listener de la tâche GetFromUrl: il y a eu une erreur */
    public void onGetFromUrlError (Exception e) {
        Log.i("infoo", "Erreur " + e);
        System.out.println("Erreur onGetFromUrlError " + e);
    } // onGetFromUrlError

    private void getInfo(){new GetFromUrl(this, 1).execute(GETTAG);}

} // MassageActivity
